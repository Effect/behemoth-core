DROP TABLE IF EXISTS `cheat_first_report`;

CREATE TABLE `cheat_first_report` (
  `guid` INT(30) DEFAULT NULL,
  `name` VARCHAR(30) DEFAULT NULL,
  `latency` INT(5) DEFAULT NULL,
  `time` VARCHAR(60) DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Table structure for table `cheat_reports` */

DROP TABLE IF EXISTS `cheat_reports`;

CREATE TABLE `cheat_reports` (
  `guid` INT(30) DEFAULT NULL,
  `name` VARCHAR(20) DEFAULT NULL,
  `latency` INT(10) DEFAULT NULL,
  `mapid` INT(5) DEFAULT NULL,
  `position_x` DOUBLE DEFAULT NULL,
  `position_y` DOUBLE DEFAULT NULL,
  `position_z` DOUBLE DEFAULT NULL,
  `report` VARCHAR(100) DEFAULT NULL,
  `time` VARCHAR(100) DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Table structure for table `cheat_temp_reports` */

DROP TABLE IF EXISTS `cheat_temp_reports`;

CREATE TABLE `cheat_temp_reports` (
  `guid` INT(30) DEFAULT NULL,
  `name` VARCHAR(15) DEFAULT NULL,
  `mapid` INT(10) DEFAULT NULL,
  `position_x` DOUBLE DEFAULT NULL,
  `position_y` DOUBLE DEFAULT NULL,
  `position_z` DOUBLE DEFAULT NULL,
  `report` VARCHAR(100) DEFAULT NULL,
  `time` VARCHAR(100) DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;