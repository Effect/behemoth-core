DELETE FROM `trinity_string` WHERE entry BETWEEN 1334 AND 1346;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1334', '你的当前 VIP 等级为： %i.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1335', '你的当前 梦境币 为 : %i.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1336', '玩家: %s (账号: %s) 的 VIP 等级设置为 : %i.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1337', '设置 VIP 等级失败！请检查你的输入');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1338', '玩家: %s (账号: %s) 的 梦境币 设置为 : %i.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1339', '设置 梦境币 失败！请检查你的输入');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1340', '账号: %s 的 VIP 等级设置为：%i, 梦境币 设置为: %i.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1341', '设置 VIP 等级 和 梦境币 错误，请检查你的输入.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1342', '你为玩家: %s (账号: %s) 添加了 %i 个 梦境币, 该玩家当前 梦境币 为: %i.');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1343', '增加 梦境币 失败！请检查你的输入');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1344', '|cFF90EE90[梦境之末]:|r 恭喜你, 你获得了 |cFF90EE90%i|r 个 |cFF90EE90[梦境币]|r.(在线奖励)');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1345', '|cFF90EE90[梦境之末]:|r 恭喜你, 你获得了 |cFF90EE90%i|r 点 |cFF90EE90[经验值]|r |cFF90EE90%i|r ,个 |cFF90EE90[金币]|r(在线奖励)');
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES ('1346', '|cFFFF0000你尚未学习该专业技能！|r');

REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip vip', '3', '语法: .vip vip\r\n\r\n查看你的当前 VIP 等级.');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('point', '3', '语法: .point\r\n\r\n查看你的当前 梦境币.');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip setvip', '3', '语法: .setvip [#角色名] VIP等级\r\n\r\n设置指定角色的 VIP 等级。 当#角色名没有指定时， 设置当前选中角色的VIP等级。');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip setpoint', '3', '语法: .setpoint [#角色名] 梦境币\r\n\r\n设置指定角色的梦境币. 当 #角色名 没有指定时, 设置当前选中角色的 梦境币. ');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('modify point', '3', '语法: .modify point [#角色名] 梦境币\r\n\r\n为指定角色增加 梦境币 . 当 #角色名 没有指定时 为当前选中角色增加梦境币.\r\n梦境币的值 可以为负数.');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('account set vip', '3', '语法: .account set vip [#账号] VIP等级 梦境币\r\n\r\n设置指定账号的 VIP 等级和 梦境币. 当 #账号 没有指定时, 设置当前选择角色的 VIP 等级和 梦境币.')

DELETE FROM `trinity_string` WHERE entry IN (1412,1413);
REPLACE INTO `trinity_string` (`entry`, `content_default`) VALUES ('1346', '|cFFFF0000你的 VIP 等级不够使用该项服务！|r');
REPLACE INTO `trinity_string` (`entry`, `content_default`) VALUES ('1347', '|cFFFF0000你的积分不够使用该项服务！|r');

ALTER TABLE npc_vendor
ADD VIPPointsCost INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER extendedcost;

ALTER TABLE game_event_npc_vendor
ADD VIPPointsCost INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER extendedcost;

DELETE FROM gossip_menu_option WHERE menu_id = 60000;
-- VIP NPC SHOP
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`) VALUES (997878, 0, 0, 0, 0, 0, 25548, 0, 0, 0, 'Luiz Cruis', 'VIP VENDOR', '', 60000, 80, 80, 0, 129, 1, 1.14286, 1, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'SmartAI', 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '');
UPDATE creature_template SET faction = 35 WHERE entry = 997878;

REPLACE INTO npc_vendor (entry, item, VIPPointsCost) VALUES
(997878, 45287, 200),
(997878, 45864, 300);
