﻿/*
+<--------------------------------------------------------------------------->
+- Developer(s): Ghostcrawler336
+- Made By??
+- Complete: %0
+- ScriptName: 'World chat'
+- Comment: Untested ingame.
+<--------------------------------------------------------------------------->
+*/

#include "ScriptPCH.h"
#include "Config.h"
#include "Chat.h"
#include "Common.h"

/* Colors */
#define MSG_COLOR_ORANGE "|cffFFA500"
#define MSG_COLOR_DARKORANGE "|cffFF8C00"
#define MSG_COLOR_RED "|cffFF0000"
#define MSG_COLOR_LIGHTRED "|cffD63931"
#define MSG_COLOR_ROYALBLUE "|cff4169E1"
#define MSG_COLOR_LIGHTBLUE "|cffADD8E6"
#define MSG_COLOR_YELLOW "|cffFFFF00"
#define MSG_COLOR_GREEN "|cff008000"
#define MSG_COLOR_PURPLE "|cffDA70D6"
#define MSG_COLOR_WHITE  "|cffffffff"
#define MSG_COLOR_SUBWHITE  "|cffbbbbbb"

/* Ranks */
#define ADMINISTRATOR "Admin"
#define HEADGM "Head GM"
#define GAMEMASTER "GM"
#define DEVELOPER "Developer"
#define OWNER "Owner"
#define VIP "Vip"
#define PLAYER "Player"
#define EVENTM "Event Master"




class World_Chat : public CommandScript
{
public:
    World_Chat() : CommandScript("World_Chat") { }


    static bool HandleWorldChatCommand(ChatHandler * pChat, const char * msg)
    {
        // uint32 sjchatmoney = 1000;
        if (!*msg)
            return false;
        Player * player = pChat->GetSession()->GetPlayer();
        char message[1024];
        bool EnableProfessions = sConfigMgr->GetBoolDefault("World.sjchat.Enable", true);
        //bool EnableSecondarySkills = sConfigMgr->GetBoolDefault("World.sjchat.Costs", true);

        if (sConfigMgr->GetBoolDefault("World.sjchat.Enable", true))
        {
            if (player->getLevel() < (sConfigMgr->GetIntDefault("World.sjchat.Level", 0)))
            {
                ChatHandler(player->GetSession()).SendSysMessage("Not enough level！");
                return true;
            }
            else if (player->GetPoint() < (sConfigMgr->GetIntDefault("World.sjchat.Costs", 0)))
            {
                ChatHandler(player->GetSession()).SendSysMessage("Not enough points！");
                return true;
            }


            /*// if (pChat->GetSession()->GetPlayer()->GetMoney() < (sWorld->getIntConfig(CONFIG_WORLD_CHAT_COSTS)))//判断金钱是否满足
            if (pChat->GetSession()->GetPlayer()->GetPoint() < (sWorld->getIntConfig(CONFIG_WORLD_CHAT_COSTS)))//判断金钱是否满足
            {
            pChat->GetSession()->GetPlayer()->GetSession()->SendNotification(sObjectMgr->GBKToUtf8("你钱不够，世界喊话需要%dG"), sWorld->getIntConfig(CONFIG_WORLD_CHAT_COSTS));
            return true;
            }

            // else if (sWorld->getBoolConfig(CONFIG_WORLD_CHAT_ENABLE) == false)*/


            switch (player->GetSession()->GetSecurity())
            {
                case SEC_PLAYER:
                    snprintf(message, 1024, "nlablalda %s%s blabla %s%s|r", MSG_COLOR_GREEN, player->GetName().c_str(), MSG_COLOR_LIGHTBLUE, msg);
                    //				pChat->GetSession()->GetPlayer()->ModifyMoney(-int32(sjchatmoney));
                    //	pChat->GetSession()->GetPlayer()->ModifyMoney(sWorld->getIntConfig(CONFIG_WORLD_CHAT_COSTS) * -1);
                    pChat->GetSession()->GetPlayer()->ModifyPoint(-(sConfigMgr->GetIntDefault("World.sjchat.Costs", 0)));
                    sWorld->SendGlobalText(message, NULL);

                    break;
            }
        }

        return true;
    }


    ChatCommand * GetCommands() const
    {
        static ChatCommand HandleWorldChatCommandTable[] =
        {
            { "sj", rbac::RBAC_PERM_COMMAND_WORLD_CHAT, true, &HandleWorldChatCommand, "", NULL },
            { NULL, 0, false, NULL, "", NULL }
        };
        return HandleWorldChatCommandTable;
    }

};

void AddSC_World_Chat()
{
    new World_Chat;
}